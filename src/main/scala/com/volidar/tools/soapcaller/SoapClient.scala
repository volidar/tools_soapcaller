package com.volidar.soapcaller

import scala.io.Source
import scala.util.Try

object SoapClient {
  def main(args: Array[String]) {
//    val target = "http://localhost:8080/merapi-ws-v8/easyplex/services/ServiceTreeService"
    val target = "http://merapi-perf.iot40systems.com:8080/merapi-ws-v8/easyplex/services/ServiceTreeService"

    val request = if(true) {
      """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://easyplex.com/merapi/v8/common/types" xmlns:par="http://easyplex.com/merapi/v8/services/servicetreeservice/parameters">
        |   <soapenv:Header>
        |      <typ:RemcoHeader>
        |      </typ:RemcoHeader>
        |   </soapenv:Header>
        |   <soapenv:Body>
        |      <par:GetServiceTree>
        |         <par:addCustomNodeColorInformation>true</par:addCustomNodeColorInformation>
        |         <par:addAdditionalStatusInformation>true</par:addAdditionalStatusInformation>
        |      </par:GetServiceTree>
        |   </soapenv:Body>
        |</soapenv:Envelope>""".stripMargin
    } else {
      """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://easyplex.com/merapi/v8/common/types"
        |      xmlns:par="http://easyplex.com/merapi/v8/services/servicetreeservice/parameters">
        |   <soapenv:Header>
        |      <typ:RemcoHeader>
        |      </typ:RemcoHeader>
        |   </soapenv:Header>
        |   <soapenv:Body>
        |      <par:GetServiceTree>
        |      </par:GetServiceTree>
        |   </soapenv:Body>
        |</soapenv:Envelope>""".stripMargin
    }

    val startTime = System.currentTimeMillis()
    sendMessage(target, request) match {
      case Left(response) =>
        val responseTime = System.currentTimeMillis()
        println(s"Request Duration: ${responseTime - startTime}")
        println(beautify(response))
        println(s"Request Duration: ${responseTime - startTime}")
        println(s"Formatting Duration: ${System.currentTimeMillis() - responseTime}")
      case Right((ex, message)) =>
        ex.printStackTrace(System.err)
        System.err.println(message)
    }
  }

  def sendMessage(target: String, requestBody: String): Either[String, (Exception, Option[String])] = {
    val url = new java.net.URL(target)
    val requestBytes = requestBody.getBytes
    val connection = url.openConnection.asInstanceOf[java.net.HttpURLConnection]
    try {
      connection.setRequestMethod("POST")
      connection.setDoOutput(true)
      connection.setRequestProperty("Content-Length", requestBytes.length.toString)
      connection.setRequestProperty("Content-Type", "text/xml")
      connection.setRequestProperty("Authorization", "Basic cm9vdDptZXJhcGk=")
      connection.getOutputStream.write(requestBytes)
      connection.getOutputStream.close
      Left(Source.fromInputStream(connection.getInputStream, "UTF-8").mkString)
    }
    catch {case ex: Exception => Right((ex, Try(Source.fromInputStream(connection.getErrorStream, "UTF-8").mkString).toOption))}
  }

  def beautify(source: String): String = {
    val tags1 = source.split("<")
    val tags2 = (tags1.head.trim #:: tags1.tail.map('<' + _.trim).toStream).filter(_.nonEmpty)
    tags2.foldLeft((List.empty[String], 0, false)) {
      case ((lastLine :: accumulator, indentation, false), close) if close.startsWith("</") =>
        val newIndentation = math.max(0, indentation - 1)
        (lastLine + close :: accumulator, newIndentation, true)
      case ((accumulator, indentation, true), close) if close.startsWith("</") =>
        val newIndentation = math.max(0, indentation - 1)
        ("  " * newIndentation + close :: accumulator, newIndentation, true)
      case ((accumulator, indentation, _), closed) if closed.endsWith("/>") =>
        ("  " * indentation + closed :: accumulator, indentation, true)
      case ((accumulator, indentation, _), open) =>
        ("  " * indentation + open :: accumulator, indentation + 1, false)
    }._1.reverse.splitAt(1024) match {
      case (first, last) => first.mkString("\r\n") + "\r\n..."/*last.mkString*/ + s"\r\n<!-- Line Count: ${first.length + last.length} -->"
    }
  }
}
